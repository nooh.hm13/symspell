Here is a sample README file for using SymSpell to correct spelling errors in both US English and UK English:

# SymSpell Spelling Correction

This is a guide for using SymSpell to correct spelling errors in both US English and UK English. SymSpell is an open-source Python library that uses the Damerau-Levenshtein distance algorithm to suggest corrections for misspelled words.

## Installation

To install SymSpell, run the following command:

```
pip install symspellpy
```
## Frequency Count
Frequency count is done with the help of Google ngrams 'https://storage.googleapis.com/books/ngrams/books/datasetsv2.html'. The frequency count is done for both US English and UK English and the count is  stored in "en_us.txt" for US English and "en_uk.txt" for UK English.


## Usage

1. Import the SymSpell package:

   ```python
   from symspellpy import SymSpell
   ```

2. Create a SymSpell object with a maximum edit distance of 2 and a prefix length of 7:

   ```python
   symspell = SymSpell(max_dictionary_edit_distance=2, prefix_length=7)
   ```

3. Load the US English dictionary file:

   ```python
   dictionary_path = "en_us.txt"
   symspell.load_dictionary(dictionary_path, term_index=0, count_index=1)
   ```

4. Load the UK English dictionary file:

   ```python
   dictionary_path = "en_uk.txt"
   symspell.load_dictionary(dictionary_path, term_index=0, count_index=1)
   ```

5. Correct a misspelled word using the `lookup` method:

   ```python
   misspelled_word = "mispelled"
   suggestions = symspell.lookup(misspelled_word, max_edit_distance=2)
   if suggestions:
       corrected_word = suggestions[0].term
   else:
       corrected_word = misspelled_word
   ```

   Note that we don't specify the verbosity level here, so SymSpell will return the closest suggestion regardless of frequency or distance.

8. Input

    The input text file(input.txt) contains article with some misspelled words in them.


7. Output

    The output file contains corrected words. For US-English is stored in "output-en_us.txt"  and the "output-en_uk.txt".

## Custom Frequency Dictionaries

If you have a custom frequency dictionary for either US or UK English, you can use it with SymSpell by loading it using the `load_dictionary` method as described above. Simply provide the path to your dictionary file, and specify the term index (column containing the words) and count index (column containing the frequency counts).

## Conclusion

In this guide, we've shown you how to use SymSpell to correct spelling errors in both US English and UK English. By following these steps, you can ensure that your spelling corrections are accurate and consistent across different English dialects.
