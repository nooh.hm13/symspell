import pkg_resources
from symspellpy import SymSpell,Verbosity

def spell_check(input_term):
    sym_spell = SymSpell(max_dictionary_edit_distance=2, prefix_length=7)
    dictionary_path = pkg_resources.resource_filename(
        "symspellpy", "frequency_dictionary_en_82_765.txt"
    )
    bigram_path = pkg_resources.resource_filename(
        "symspellpy", "frequency_bigramdictionary_en_243_342.txt"
    )
    # term_index is the column of the term and count_index is the
    # column of the term frequency
    sym_spell.load_dictionary(dictionary_path, term_index=0, count_index=1)
    sym_spell.load_bigram_dictionary(bigram_path, term_index=0, count_index=2)

    # lookup suggestions for multi-word input strings (supports compound
    # splitting & merging)

    lookup_dict={}
    # max edit distance per lookup (per single word, not per whole input string)
    letter_count=0
    key_count=0
    for word in input_term.split(' '):
        start_count= letter_count+1
        suggestions = sym_spell.lookup(
        word, Verbosity.CLOSEST, max_edit_distance=2, transfer_casing=True
    )
        letter_count += len(i)
        suggest_lt=[]
        for suggestion in suggestions:
            if suggestion != word:
                s=suggestion
                suggest_lt.append(suggestion.term)
                #print(suggestion)
        if len(suggest_lt)>2:
            word_dict={}
            word_dict['original']=word
            word_dict['suggestions']=suggest_lt
            word_dict['position_start']=start_count
            word_dict['position_end']=letter_count
            lookup_dict[word]=word_dict
    return lookup_dict
