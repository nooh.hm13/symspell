import gzip
frequencies={}
ngrams=[]
counts=[]
count=1

with gzip.open('gram.gz', 'rt') as f:
    for line in f:
        words = line.split()
        
        for word in words:
            if "_" in word:
                 ind= word.index('_')
                 word=word[0:ind]
            if count%4==1:
                ngrams.append((word))
                print(word)
            elif count%4==0:
                try:
                    counts.append(int(word))
                except ValueError:
                    continue
                print(words)
            count=count+1

frequencies={}
for i in range(len(counts)):
    if ngrams[i] not in frequencies:
        frequencies[ngrams[i]] = int(counts[i])
    else:
        frequencies[ngrams[i]] += int(counts[i])


with open('1gram_frequency.txt','a') as f:
     for key, value in frequencies.items():
         f.write(f'{key}: {value} \n')
        
