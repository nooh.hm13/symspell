

import pkg_resources
from symspellpy import SymSpell, Verbosity

# Load a dictionary containing a large number of words
dictionary_path = 'en_us.txt' # for UK English replace the 'en_us' with en_uk
sym_spell = SymSpell(max_dictionary_edit_distance=2, prefix_length=7)
sym_spell.load_dictionary(dictionary_path, term_index=0, count_index=1)

# Define a function to perform spelling correction
def correct_spelling(text):
    suggestions = sym_spell.lookup_compound(text, max_edit_distance=2)
    corrected_text = ""
    for suggestion in suggestions:
        corrected_text += suggestion.term + " "
    return corrected_text.strip()

# Open the input file and read the contents
with open('input.txt', 'r') as f:
    input_text = f.read()

# Perform spelling correction on the input text
corrected_text = correct_spelling(input_text)

# Open the output file and write the corrected text to it
with open('output-en_us.txt', 'w') as f: # for UK English replace the 'en_us' with en_uk
    f.write(corrected_text)

