from spellchecker import SpellChecker

# Create a SpellChecker object
spell = SpellChecker()

# Load a dictionary of correct words
dictionary_path = "en_uk.txt"
encoding = "latin-1"
spell.word_frequency.load_text_file(dictionary_path,encoding=encoding)


# Input string with incorrect spelling
with open('input.txt','r') as f:
    ip=f.read()
input_string = ip
# Correct the spelling using SpellChecker
    corrected_string = []
    for word in input_string.split():
        encoded_word = word.encode(encoding)
        # Check if the word is misspelled
        if not spell.correction(encoded_word) == encoded_word:
            # If misspelled, add the corrected word to the list
            corrected_string.append(spell.correction(encoded_word))
        else:
            # If correctly spelled, add the original word to the list
            corrected_string.append(word)
    op_string = ' '.join(str(item) for item in corrected_string)


with open('output-uk_spellchecker.txt','w') as f:
    f.write(op_string)
